/**
 * Implementação de Interface - Figura Geométrica
 * @author George Mendonça
 * Data: 22/09/2016
 * Atualização:13/10/2016
 */
interface IFiguraGeomatrica {

	public String nome = "";
	public double area = 0.0;
	public double volume = 0.0;
	
	public String getNome();
	public void setNome(String n);
	
	public double getArea();
	public void setArea(double ar);
	
	public double getVolume();
	public void setVolume(double vm);
}
