/**
 * Implementação de uma classe Conta Bancária simples - Testando a classe
 * @author George Mendonça
 * Data: 13/10/2016
 * Atualização:
 * */
public class AppConta {
		
	public static void main(String[] args) {
		Conta conta = new Conta();
		conta.setTitular("Rei Arthur");
		conta.setNumero(4854657);
		conta.setSaldo(10000.00);
		conta.extrato(conta);
		conta.depositar(150.00);
		conta.depositar(1500.00);
		conta.depositar(700.00);
		conta.extrato(conta);
		conta.sacar(4000.00);
		conta.sacar(1000.00);
		conta.depositar(500.00);
		conta.extrato(conta);
	}
}
